namespace import oo::*

class create SquareMatrix {
    variable m_lines
    variable m_size

    constructor {size} {
        set m_size $size

        set line {}

        for {set i 0} {$i < $m_size} {incr i} {
            lappend line 0
        }

        for {set i 0} {$i < $m_size} {incr i} {
            set m_lines($i) $line
        }
    }

    method print {} {
        parray m_lines
    }

    method cell {value li ci} {
        set  linecopy     $m_lines($li)
        lset linecopy     $ci $value
        set  m_lines($li) $linecopy
    }
    
    method line {li} {
        return $m_lines($li)
    }

    method column {ci} {
        set column {}
        for {set i 0} {$i < $m_size} {incr i} {
            lappend column [lindex $m_lines($i) $ci]
        }
        return $column
    }

    method check_bounds {li ci} {
        if {   (0 <= $li && $li < $m_size)
            && (0 <= $ci && $ci < $m_size)} {
            return true
        }
        return false
    }
    
    method diagonal {li ci vway hway} {
        set diagonal {}

        if {"up" == $vway} {
            set vstep -1
        } elseif {"down" == $vway} {
            set vstep 1
        }

        if {"left" == $hway} {
            set hstep -1
        } elseif {"right" == $hway} {
            set hstep 1
        }

        while {[my check_bounds $li $ci]} {
            lappend diagonal [lindex $m_lines($li) $ci]

            incr li $vstep
            incr ci $hstep
        }

        return $diagonal
    }

    destructor {
        $m_lines destroy
        $m_size  destroy
    }
}
